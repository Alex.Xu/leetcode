class Solution:
    # @return an integer
    def atoi(self, str):
        Max=2147483647
        i=0
        n=len(str)
        va=0
        op=True
        first=-1
        while i<n:
            #print i
            if str[i]==" ":
                i+=1
            elif str[i]=="+":
                if not first==-1:
                    return 0
                else:
                    first=i
                op=True
                i+=1
            elif str[i]=="-":
                #print "dd"
                if not first==-1:
                    #print "ddd"
                    return 0
                else:
                    first=i
                op=False
                i+=1
            elif    str[i]>='0' and str[i]<='9':
                if first==-1:
                    first=i
                va=va*10+ord(str[i])-48
                i+=1
            else:
                break
            #i+=1
        if va>Max:
            va=0
        if not op:
            va=-va
        #print op
        return va   
if __name__=="__main__":
    so=Solution()
    print so.atoi("   -04f")