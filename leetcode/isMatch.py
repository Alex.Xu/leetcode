class Solution:
    # @return a boolean
    
    def match(self,ch,p,j):
        m=len(p)
        cu=False
        while j<m:
            c=p[j]
            if c==ch or c==".":
                j+=1
                if j<m:
                    c2=p[j]
                    if c2=="*":
                        j-=1
                        cu=True
                return [j,cu]
            else:
                j+=1
        return [-1,cu]
        
    def isMatch(self, s, p):
        m=len(s)
        n=len(p)
        j=0
        for i in range(0,m):
            chs=s[i]
            [j,cu]=self.match(chs,p,j)
            print j
            if j==-1:
                return False
        if cu:
            j+=2
        if  j==n:
            return True
        else:
            return False
    
if __name__=="__main__":
    so=Solution()
    print so.isMatch("aaa","a*a")