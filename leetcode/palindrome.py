class Solution:
    # @return a string
    def longestPalindrome(self, s):
        charlist=list(s)
        k='#'.join(charlist)
        k="#"+k+"#"
        #print k
        
        n=len(k)
        _index=dict()
        for i in range(0,n):
            if not _index.has_key(k[i]):
                _index[k[i]]=[i]
            else:
                _index[k[i]]+=[i]
        
        #print _index
        P=[0]*n
        
        i=0
        for i in range(0,n):
            #j=i+1
            for poj in _index[k[i]]:
                if poj<(i+poj)/2:
                    if not (i+poj)%2:
                        zhou=(i+poj)/2
                        #print zhou,j
                        if P[zhou]==i-zhou-1:
                            P[zhou]+=1
                else:
                    break
        #print P   
        maxlen=max(P)
        zhuindex=P.index(maxlen)
        return s[(zhuindex-maxlen+1)/2:(zhuindex+maxlen+1)/2]
        
if __name__=="__main__":
    so=Solution()
    print so.longestPalindrome("miycvxmqggnmmcwlmizfojwrurwhwygwfykyefxbgveixykdebenzitqnciigfjgrzzbtgeazyrbiirmejhdwcgjzwqolrturjlqpsgunuqerqjevbheblmbvgxyedxshswsokbhzapfuojgyfhctlaifrisgzqlczageirnukgnmnbwogknyyuynwsuwbumdmoqwxprykmazghcpmkdcjduepjmjdxrhvixxbfvhybjdpvwjbarmbqypsylgtzyuiqkexgvirzylydrhrmuwpmfkvqllqvekyojoacvyrzjevaupypfrdguhukzuqojolvycgpjaendfetkgtojepelhcltorueawwjpltehbbjrvznxhahtuaeuairvuklctuhcyzomwrrznrcqmovanxmiyilefybkbveesrxkmqrqkowyrimuejqtikcjfhizsmumajbqglxrvevexnleflocxoqgoyrzgqflwiknntdcykuvdcpzlakljidclhkllftxpinpvbngtexngdtntunzgahuvfnqjedcafzouopiixw")