def location(p,ch):
    m=len(p)
    list=[]
    for i in range(0,m):
        if ch==p[i]:
            list+=[i]
    return list

def find(s,word,i=0):
    #print s,word
    m=len(s)
    
    n=len(word)
    if n==0:
        return i
    j=0
    #i=0
    while i<m:
        if s[i]==word[j] or word[j]==".":
            i+=1
            j+=1
            if j==n:
                return i-j
        else:
            i+=1
            j=0
    return -1

class Solution:


    def partition(self,p):
        m=len(p)
        i=0
        star=location(p,'*')
        dot=location(p,".")
        #return star
        stargroup=[]
        restgroup=[]
        parts=[p]
        for i in range(0,len(star)):
            stargroup+=[p[star[i]-1:star[i]+1]]
        Pfocus=p
        for i in range(0,len(stargroup)):
            parts=Pfocus.partition(stargroup[i])
            #print parts
            restgroup+=[parts[0]]
            Pfocus=parts[-1]
        #print restgroup,parts
        if not parts[-1]=="":
            restgroup+=[parts[-1]]
        return restgroup,stargroup  

        
    def checkstar(self,p,seq):
        m=len(seq)
        if m==0:
            return True
        letter=p[0]
        for i in range(0,m):
            if not (seq[i]==letter or letter=="."):
                return False
        return  True

    
    def isMatch(self,s,p):
        if len(p)==0:
            return False
        [listN,listS]=self.partition(p)
        print listN,listS
        n=len(listN)
        m=len(listS)
        j=0
        index=0
        
        word1=listN[0]
        index=find(s,word1)
        if index==-1:
            return False
        #else:
            
        
        for i in range(1,n):
            word=listN[i]
            ind=find(s,word,index+len(word1))
            print ind
            if not ind==-1:
                starseg=s[index+len(word1):ind]
                if self.checkstar(listS[j],starseg):
                    #print "ddd"
                    j+=1
                    index=ind
                    word1=word
            else:
                return False
        if m>0:
            starseg=s[index+len(word1):]
            #print starseg
            if self.checkstar(listS[-1],starseg):
                index+=len(starseg)
        #print index
        if index+len(word1)==len(s):
            return True
        else:
            return False
        


if __name__=="__main__":
    so=Solution()
    print so.isMatch("aaa", "ab*a*a")